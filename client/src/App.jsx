import React, { useState, useEffect } from 'react';
import Groups from './components/Groups/Groups';
import Menu from './components/Menu/Menu';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';
import Links from './components/Links/Links';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './redux/rootReducer';

const store = createStore(rootReducer, applyMiddleware(thunk));

function App(props) {
  return (
    <div className="container">
      <Provider store={ store }>
        <Router>
          <Switch>
            <Route path={['/groups', '/']} exact >
              <Menu />
              <Groups />
            </Route>
            <Route path={['/groups/:groupId/links', '/', '/links']} exact >
              <Menu />
              <Links />
            </Route>
          </Switch>
        </Router>
      </Provider>
    </div>
  );
}

export default App;
