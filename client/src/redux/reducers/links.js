import { ADD_LINK_FAILURE, ADD_LINK_STARTED, ADD_LINK_SUCCESS, GET_LINKS_FAILURE, GET_LINKS_STARTED, GET_LINKS_SUCCESS } from "../const/const";

const initialState = {
    links: [],
    linksLoading: false,
    linksLoadError: null,
    linkAdded: true,
    linkAddError: null,
};

const linksReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_LINKS_STARTED:
            return {
                ...state,
                linksLoading: true,
            }
        case GET_LINKS_SUCCESS:
            return {
                ...state,
                linksLoading: false,
                links: action.payload,
            }
        case GET_LINKS_FAILURE:
            return {
                ...state,
                linksLoading: false,
                linksLoadError: action.payload,
            }
        case ADD_LINK_STARTED:
            return {
                ...state,
                linkAdded: false,
            }
        case ADD_LINK_SUCCESS:
            console.log('add link success');
            return {
                ...state,
                linkAdded: true,
                links: [...state.links, action.payload],
            }
        case ADD_LINK_FAILURE:
            return {
                ...state,
                linkAdded: true,
                linkAddError: action.payload,
            }
        default:
            return state;
    }
}

export default linksReducer;