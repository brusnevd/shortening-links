import { ADD_GROUP_FAILURE, ADD_GROUP_STARTED, ADD_GROUP_SUCCESS, GET_GROUPS_FAILURE, GET_GROUPS_STARTED, GET_GROUPS_SUCCESS } from "../const/const";

const initialState = {
    groups: [],
    groupsLoading: false,
    groupsLoadError: null,
    groupAdded: true,
    groupAddError: null,
};

const groupsReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_GROUPS_STARTED:
            return {
                ...state,
                groupsLoading: true,
            }
        case GET_GROUPS_SUCCESS:
            return {
                ...state,
                groupsLoading: false,
                groups: action.payload,
            }
        case GET_GROUPS_FAILURE:
            return {
                ...state,
                groupsLoading: false,
                groupsLoadError: action.payload,
            }
        case ADD_GROUP_STARTED:
            return {
                ...state,
                groupAdded: false,
            }
        case ADD_GROUP_SUCCESS:
            console.log('add link success');
            return {
                ...state,
                groupAdded: true,
                groups: [...state.groups, action.payload],
            }
        case ADD_GROUP_FAILURE:
            return {
                ...state,
                groupAdded: true,
                groupAddError: action.payload,
            }
        default:
            return state;
    }
}

export default groupsReducer;