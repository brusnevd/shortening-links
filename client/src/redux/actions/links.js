import {
    ADD_LINK_FAILURE,
    ADD_LINK_STARTED,
    ADD_LINK_SUCCESS,
    API_URL,
    GET_LINKS_FAILURE,
    GET_LINKS_STARTED,
    GET_LINKS_SUCCESS,
} from "../const/const";

export const getLinks = () => {
    return (dispatch) => {
        dispatch(getLinksStarted());

        fetch(`${API_URL}/links`)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    dispatch(getLinksSuccess(result));
                },
                (error) => {
                    dispatch(getLinksFailure(error));
                }
            ).catch((error) => {
                dispatch(getLinksFailure(error));
            });
    }
};

const getLinksStarted = () => ({
    type: GET_LINKS_STARTED,
})

const getLinksSuccess = (users) => ({
    type: GET_LINKS_SUCCESS,
    payload: users,
})

const getLinksFailure = (error) => ({
    type: GET_LINKS_FAILURE,
    payload: error,
})

// add link actions

export const addLink = (linkInfo) => {
    return (dispatch) => {
        dispatch(addLinkStarted());

        const newLink = {...linkInfo };

        console.log('dispatch link add');

        fetch(`${API_URL}/links`, {
                method: 'POST',
                body: JSON.stringify(newLink),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            })
            .then(data => data.json())
            .then((res) => {
                console.log('dispatch link add success');
                dispatch(addLinkSuccess(res));
            })
            .catch((error) => {
                dispatch(addLinkFailure(error));
            });
    }
};

const addLinkStarted = () => ({
    type: ADD_LINK_STARTED,
})

const addLinkSuccess = (link) => ({
    type: ADD_LINK_SUCCESS,
    payload: link,
})

const addLinkFailure = (error) => ({
    type: ADD_LINK_FAILURE,
    payload: error,
})