import { ADD_GROUP_FAILURE, ADD_GROUP_STARTED, ADD_GROUP_SUCCESS, API_URL, GET_GROUPS_FAILURE, GET_GROUPS_STARTED, GET_GROUPS_SUCCESS } from "../const/const";

export const getGroups = () => {
    return (dispatch) => {
        dispatch(getGroupsStarted());

        fetch(`${API_URL}/groups`)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result);
                    dispatch(getGroupsSuccess(result));
                },
                (error) => {
                    dispatch(getGroupsFailure(error));
                }
            ).catch((error) => {
                dispatch(getGroupsFailure(error));
            });
    }
};

const getGroupsStarted = () => ({
    type: GET_GROUPS_STARTED,
})

const getGroupsSuccess = (users) => ({
    type: GET_GROUPS_SUCCESS,
    payload: users,
})

const getGroupsFailure = (error) => ({
    type: GET_GROUPS_FAILURE,
    payload: error,
})

// add link actions

export const addGroup = (linkInfo) => {
    return (dispatch) => {
        dispatch(addGroupStarted());

        const newLink = {...linkInfo };

        console.log('dispatch link add');

        fetch(`${API_URL}/groups`, {
                method: 'POST',
                body: JSON.stringify(newLink),
                headers: {
                    'Content-type': 'application/json; charset=UTF-8',
                },
            })
            .then(data => data.json())
            .then((res) => {
                console.log('dispatch link add success');
                dispatch(addGroupSuccess(res));
            })
            .catch((error) => {
                dispatch(addGroupFailure(error));
            });
    }
};

const addGroupStarted = () => ({
    type: ADD_GROUP_STARTED,
})

const addGroupSuccess = (link) => ({
    type: ADD_GROUP_SUCCESS,
    payload: link,
})

const addGroupFailure = (error) => ({
    type: ADD_GROUP_FAILURE,
    payload: error,
})