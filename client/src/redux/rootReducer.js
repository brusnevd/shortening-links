import groupsReducer from "./reducers/groups";
import linksReducer from "./reducers/links";

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    groupsInfo: groupsReducer,
    linksInfo: linksReducer,
});

export default rootReducer;
