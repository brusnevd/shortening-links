import React from "react";
import { Link } from "react-router-dom";

import './menu.scss';

function Menu(props) {
    return (
        <div className="menu">
            <div className="logo">
                <img src="/public/img/logo.jpg" alt="logo" />
            </div>
            <nav className="menu-container">
                <ul className="menu-list">
                    <li className="menu-item"><Link to="/groups">Groups</Link></li>
                    <li className="menu-item"><Link to="/links">Links</Link></li>
                </ul>
            </nav>
            <div className="avatar">
                <img src="/public/img/avatar.png" alt="avatar" />
            </div>
        </div>
    )
}

export default Menu;