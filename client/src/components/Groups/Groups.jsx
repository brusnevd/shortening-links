import React, { useEffect, useRef, useCallback, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { addGroup, getGroups } from "../../redux/actions/groups";
import { getLinks } from "../../redux/actions/links";
import Modal from "../modal/Modal";

import './groups.scss';

function Groups(props) {
    const [modalInfo, setModalInfo] = useState({ isOpen: false, content: (<div></div>), title: 'Album', okHandler: () => { /** */ } });

    const dispatch = useDispatch();

    const groupsInfo = useSelector((state) => state.groupsInfo);
    const linksInfo = useSelector((state) => state.linksInfo);

    const groupTitleInput = useRef(null);

    useEffect(() => {
        dispatch(getGroups());
        dispatch(getLinks());
    }, []);

    const addGroupHandler = useCallback((e) => {
        const newGroup = {
            title: groupTitleInput.current.value,
        };

        dispatch(addGroup(newGroup));

        setModalInfo((prev) => ({
            ...prev,
            isOpen: false,
        }));
    }, [dispatch]);

    const addGroupClickHandler = useCallback((e) => {
        setModalInfo((prev) => ({
            ...prev,
            okHandler: addGroupHandler,
            isOpen: true,
            content: (
                <div className="title-container">
                    <div className="input">
                        <label htmlFor="link-title">Title:</label>
                        <input type="text" id="link-title" ref={groupTitleInput} />
                    </div>
                </div>
            ),
            title: 'New Link',
        }));
    });

    const { groups, groupsLoading, groupsLoadError, groupAdded } = groupsInfo;
    const { links, linksLoading, linksLoadError } = linksInfo;

    groups.forEach((group) => {
        group.links = [];
        links.forEach((link) => {
            if (link.groupId === group.id) {
                group.links.push(link);
            }
        });
    });

    if (groupsLoading || linksLoading) {
        return <div className="">Loading...</div>
    } else if (groupsLoadError) {
        return <div className="">{ groupsLoadError }</div>
    } else if (linksLoadError) {
        return <div className="">{ linksLoadError }</div>
    } else {
        return (
            <div className="groups">
                <Modal open={modalInfo.isOpen} title={modalInfo.title} onClose={() => { setModalInfo((prev) => ({ ...prev, isOpen: false, })); }} okHandler={modalInfo.okHandler} >
                    {modalInfo.content}
                </Modal>
                <header>
                    <div className="title">
                        All groups
                        <div className="count-groups">number - { groups.length }</div>
                    </div>
                    <div className={ "btn-add-group " + (!groupAdded ? 'disabled' : '') } onClick={groupAdded ? addGroupClickHandler : null}>Add group</div>
                </header>
                <div className="fields-information">
                    <div className="title">Title</div>
                    <div className="moves">Moves</div>
                    <div className="links">links</div>
                </div>
                {
                    groups.map((group) => (
                        <div className="group-item" key={ group.id }>
                            <div className="title">{ group.title }</div>
                            <div className="moves">{ group.moves }</div>
                            <div className="links">
                                <Link to={`groups/${group.id}/links`}>links</Link> - { group.links.length }
                            </div>
                        </div>
                    ))
                }
            </div>
        )
    }
}

export default Groups;