import React, { useEffect, useState, useCallback, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { getGroups } from "../../redux/actions/groups";
import { addLink, getLinks } from "../../redux/actions/links";
import Modal from "../modal/Modal";

import './links.scss';

function Links(props) {
    const [modalInfo, setModalInfo] = useState({ isOpen: false, content: (<div></div>), title: 'Album', okHandler: () => { /** */ } });

    const dispatch = useDispatch();
    const urlParams = useParams();

    const linksInfo = useSelector((state) => state.linksInfo);
    const groupsInfo = useSelector((state) => state.groupsInfo);

    const linkTitleInput = useRef(null);
    const linkHrefInput = useRef(null);
    const linkGroupInput = useRef(null);
    
    useEffect(() => {
        dispatch(getLinks());
        dispatch(getGroups());
    }, [dispatch]);

    const addLinkHandler = useCallback((e) => {
        const newLink = {
            title: linkTitleInput.current.value,
            "full-link": linkHrefInput.current.value,
            groupId: linkGroupInput.current.value,
        };

        dispatch(addLink(newLink));

        setModalInfo((prev) => ({
            ...prev,
            isOpen: false,
        }));
    }, [dispatch]);

    const addLinkClickHandler = useCallback((e) => {
        setModalInfo((prev) => ({
            ...prev,
            okHandler: addLinkHandler,
            isOpen: true,
            content: (
                <div className="title-container">
                    <div className="input">
                        <label htmlFor="link-title">Title:</label>
                        <input type="text" id="link-title" ref={linkTitleInput} />
                    </div>
                    <div className="input">
                        <label htmlFor="link-href">Link:</label>
                        <input type="text" id="link-href" ref={linkHrefInput} />
                    </div>
                    <div className="input">
                        <label htmlFor="link-group">Group:</label>
                        <select id="link-group" ref={linkGroupInput} >
                            {
                                groups.map((group) => (
                                    <option value={group.id} key={group.id}>{ group.title }</option>
                                ))
                            }
                        </select>
                    </div>
                </div>
            ),
            title: 'New Link',
        }));
    });

    let { links, linksLoading, linksLoadError, linkAdded } = linksInfo;
    let { groups, groupsLoading, groupsLoadError } = groupsInfo;

    console.log(links);

    if (linksLoading || groupsLoading) {
        return <div className="">Loading...</div>
    } else if (linksLoadError || groupsLoadError) {
        return <div className="">{linksLoadError || groupsLoadError}</div>
    } else {
        if (urlParams.groupId !== undefined) {
            links = links.filter((link) => link.groupId.toString() === urlParams.groupId.toString());
        }
        return (
            <div className="links-page">
                <Modal open={modalInfo.isOpen} title={modalInfo.title} onClose={() => { setModalInfo((prev) => ({ ...prev, isOpen: false, })); }} okHandler={modalInfo.okHandler} >
                    {modalInfo.content}
                </Modal>
                <header>
                    <div className="title">
                        Links
                        <div className="count-links">number - {links.length}</div>
                    </div>
                    <div className={ "btn-add-link " + (!linkAdded ? 'disabled' : '') } onClick={linkAdded ? addLinkClickHandler : null}>Add link</div>
                </header>
                <div className="fields-information">
                    <div className="status">Status</div>
                    <div className="title">Title</div>
                    <div className="moves">Moves</div>
                    <div className="full-link">full link</div>
                    <div className="short-link">short link</div>
                </div>
                {
                    links.map((link) => { console.log(link); return (
                        <div className="link-item" key={link.id}>
                            <div className="status"> <div className={"circle " + (link.isActive ? 'active' : '')} /> </div>
                            <div className="title">{link.title}<br /><span className="gray">{groups.find(({ id }) => id === link.groupId).title}</span></div>
                            <div className="moves">{link.moves}</div>
                            <div className="full-link">{link['full-link']}</div>
                            <div className="short-link"><a href={link['short-link']}>{link['short-link']}</a></div>
                        </div>
                    )})
                }
            </div>
        )
    }
}

export default Links;