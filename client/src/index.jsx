import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';;

import "./reset.scss";

const app = (
    <App />
)

ReactDOM.render(
    app,
    document.getElementById('root')
);